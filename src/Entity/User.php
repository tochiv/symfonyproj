<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository", repositoryClass=UserRepository::class)
 * @ORM\Table(name="user")
 * @UniqueEntity(fields={"email"}, message="Email is already used")
 * @UniqueEntity(fields={"login"}, message="Login is already used")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(name="email", type="string", length=180, unique=true)
     * @Assert\Unique(groups={"register"}, message="Email is already used")
     */
    private string $email;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $roles;

    /**
     * @var string The hashed password
     * @ORM\Column(name="password", type="string")
     */
    private string $password;

    /**
     * @ORM\Column(name="login", type="string", unique=true)
     * @Assert\Unique(groups={"register"}, message="Login is already used")
     */
    private string $login;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="user")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Collection $posts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Subscription", mappedBy="subscriber")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Collection $subscribers;

    /**
     * @ORM\OneToMany (targetEntity="App\Entity\Subscription", mappedBy="publisher")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Collection $publishers;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Account", inversedBy="user")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     */
    private Account $account;

    public function __construct(Account $account)
    {
        $this->posts = new ArrayCollection();
        $this->account = $account;
        $this->subscribers = new ArrayCollection();
        $this->publishers = new ArrayCollection();
    }

    public function checkSubscription(User $expectedUser, User $user): bool
    {
        foreach ($this->publishers as $publisher) {
            if (
                $publisher->getPublisher()->getId() === $expectedUser->getId()
                && $publisher->getSubscriber()->getId() === $user->getId()
            ) {
                return true;
            }
        }

        return false;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function addPost(Post $posts): self
    {
        if (!$this->posts->contains($posts)) {
            $this->posts[] = $posts;
            $posts->setUser($this);
        }

        return $this;
    }

    public function removePost(Post $posts): self
    {
        if ($this->posts->contains($posts)) {
            $this->posts->removeElement($posts);
            // set the owning side to null (unless already changed)
            if ($posts->getUser() === $this) {
                $posts->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function getAccount(): object
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @return Collection|Subscription[]
     */
    public function getSubscriber(): ?Collection
    {
        return $this->subscribers;
    }

    public function addSubscriber(Subscription $subscriber): self
    {
        if (!$this->subscribers->contains($subscriber)) {
            $this->subscribers[] = $subscriber;
            $subscriber->setSubscriber($this);
        }

        return $this;
    }

    public function removeSubscriber(Subscription $subscriber): self
    {
        if ($this->subscribers->contains($subscriber)) {
            $this->subscribers->removeElement($subscriber);
            // set the owning side to null (unless already changed)
            if ($subscriber->getSubscriber() === $this) {
                $subscriber->setSubscriber(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Subscription[]
     */
    public function getPublisher(): ?Collection
    {
        return $this->publishers;
    }

    public function addPublisher(Subscription $publisher): self
    {
        if (!$this->publishers->contains($publisher)) {
            $this->publishers[] = $publisher;
            $publisher->setPublisher($this);
        }

        return $this;
    }

    public function removePublisher(Subscription $publisher): self
    {
        if ($this->publishers->contains($publisher)) {
            $this->publishers->removeElement($publisher);
            // set the owning side to null (unless already changed)
            if ($publisher->getPublisher() === $this) {
                $publisher->setPublisher(null);
            }
        }

        return $this;
    }
}
