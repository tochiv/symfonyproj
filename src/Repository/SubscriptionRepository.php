<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Subscription;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Subscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method Subscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method Subscription[]    findAll()
 * @method Subscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class SubscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Subscription::class);
    }

    public function findSubscription(User $subscriber, User $publisher)
    {
        $qb = $this->createQueryBuilder('s');
        $expr = $qb->expr();

        return $qb
            ->andWhere($expr->eq('s.publisher', ':publisher'))
            ->andWhere($expr->eq('s.subscriber', ':subscriber'))
            ->setParameter('subscriber', $subscriber)
            ->setParameter('publisher', $publisher)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
