<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\SubscriptionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SubscriptionRepository::class)
 * @ORM\Table(name="subscription")
 */
class Subscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="subscribers")
     * @ORM\JoinColumn(name="subscriber", referencedColumnName="id")
     */
    private User $subscriber;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="publishers")
     * @ORM\JoinColumn(name="publisher", referencedColumnName="id")
     */
    private User $publisher;

    public function __construct(User $subscriber, User $publisher)
    {
        $this->subscriber = $subscriber;
        $this->publisher = $publisher;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubscriber(): ?User
    {
        return $this->subscriber;
    }

    public function setSubscriber(User $subscriber): self
    {
        $this->subscriber = $subscriber;

        return $this;
    }

    public function getPublisher(): ?User
    {
        return $this->publisher;
    }

    public function setPublisher(User $publisher): self
    {
        $this->publisher = $publisher;

        return $this;
    }
}
