<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Repository\PostRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class ProfileController extends AbstractController
{
    private UserRepository $userRepository;
    private PostRepository $postRepository;
    private PaginatorInterface $paginator;
    private EntityManagerInterface $entityManager;

    public function __construct(
        UserRepository $userRepository,
        PostRepository $postRepository,
        PaginatorInterface $paginator,
        EntityManagerInterface $entityManager
    )
    {
        $this->userRepository = $userRepository;
        $this->postRepository = $postRepository;
        $this->paginator = $paginator;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/profile/{login}", name="app_account_profile")
     */
    public function index(Request $request, string $login): Response
    {
        $profileUser = $this->userRepository->findUserByLogin($login);
        /** @var User $user */
        $user = $this->getUser();
        $userLogin = $user->getLogin();

        $query = $this->postRepository->getPost($profileUser);
        $pagination = $this->paginator->paginate(
            $query,
            $request->query->getInt('page', 1), 7
        );

        if($userLogin === $login){
            return $this->redirectToRoute('app_main_page');
        }

        return $this->render('profile/index.html.twig', [
            'dataUser' => $profileUser,
            'pagination' => $pagination,
        ]);
    }
}
