<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Account;
use App\Entity\User;
use App\Form\AccountFormType;
use App\Repository\AccountRepository;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends AbstractController
{
    private FileUploader $fileUploader;
    private EntityManagerInterface $entityManager;
    private AccountRepository $accountRepository;

    public function __construct(
        FileUploader $fileUploader,
        EntityManagerInterface $entityManager,
        AccountRepository $accountRepository
    )
    {
        $this->fileUploader = $fileUploader;
        $this->entityManager = $entityManager;
        $this->accountRepository = $accountRepository;
    }

    /**
     * @Route("/account/edit", name="app_account_edit")
     */
    public function index(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var Account $account */
        $account = $user->getAccount();

        $form = $this->createForm(AccountFormType::class, $account);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $avatarFile */
            $avatarFile = $form->get('avatar')->getData();
            if ($avatarFile) {
                $avatarFile = $this->fileUploader->upload($avatarFile);
                $account->setAvatar($avatarFile);
            }

            $this->entityManager->flush();
        }

        return $this->render('account/index.html.twig', [
            'accountForm' => $form->createView(),
            'account' => $account
        ]);
    }
}
