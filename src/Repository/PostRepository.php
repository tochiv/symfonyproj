<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function getPost(User $user)
    {
        $qb = $this->createQueryBuilder('p');
        $expr = $qb->expr();

        return $qb
            ->addSelect('p.text', 'p.image', 'p.date')
            ->innerJoin('p.user', 'u')
            ->andWhere($expr->eq('p.user', ':id'))
            ->addOrderBy('p.id', 'DESC')
            ->setParameter('id', $user);
    }
}
