<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Subscription;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SubController extends AbstractController
{
    private UserRepository $userRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(
        UserRepository $userRepository,
        EntityManagerInterface $entityManager
    )
    {
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/profile/sub/{login}", name="app_profile_sub")
     */
    public function index(string $login): Response
    {
        $publisher = $this->userRepository->findUserByLogin($login);
        /** @var User $subscriber */
        $subscriber = $this->getUser();

        $sub = new Subscription($subscriber, $publisher);
        $this->entityManager->persist($sub);
        $this->entityManager->flush();

        return $this->redirectToRoute('app_account_profile', [
            'login' => $login
        ]);
    }
}
