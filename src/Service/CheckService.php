<?php

declare(strict_types = 1);

namespace App\Service;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class CheckService
{
    public AuthorizationCheckerInterface $authChecker;

    public function __construct(AuthorizationCheckerInterface $authChecker)
    {
        $this->authChecker = $authChecker;
    }

    public function check()
    {
        return $this->authChecker->isGranted('IS_AUTHENTICATED_FULLY');
    }
}