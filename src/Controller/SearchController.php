<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotBlank;

class SearchController extends AbstractController
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/account/search", name="app_search")
     */
    public function index(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('query', SearchType::class, [
                'label' => 'Login',
                'required' => true,
                'mapped' => false,
                'constraints' => [
                    new NotBlank(
                        [
                            'message' => 'Please enter a login'
                        ]
                    )
                ]
            ])
            ->add('search', SubmitType::class, [
                'label' => 'Search'
            ])
            ->getForm();

        if (!isset($request->get('form')['query'])) {
            return $this->render('search/index.html.twig', [
                'searchForm' => $form->createView(),
            ]);
        } else {
            $query = $request->get('form')['query'];
        }

        $user = $this->userRepository->findUserByLogin($query);

        if ($user && $query !== null) {
            return $this->render('search/index.html.twig', [
                'searchForm' => $form->createView(),
                'users' => $user
            ]);
        }

        return $this->render('search/index.html.twig', [
            'searchForm' => $form->createView(),
        ]);
    }
}
