<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Repository\SubscriptionRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UnsubController extends AbstractController
{
    private SubscriptionRepository $subscriptionRepository;
    private UserRepository $userRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(
        SubscriptionRepository $subscriptionRepository,
        EntityManagerInterface $entityManager,
        UserRepository $userRepository
    )
    {
        $this->subscriptionRepository = $subscriptionRepository;
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("profile/unsub/{login}", name="app_profile_unsub")
     */
    public function index(string $login)
    {
        $publisher = $this->userRepository->findUserByLogin($login);
        /** @var User $subscriber */
        $subscriber = $this->getUser();
        $subscription = $this->subscriptionRepository->findSubscription($subscriber, $publisher);

        $this->entityManager->remove($subscription);
        $this->entityManager->flush();

        return $this->redirectToRoute('app_account_profile',[
            'login' => $login
        ]);
    }
}
