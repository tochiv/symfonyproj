<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Account;
use App\Entity\Post;
use App\Entity\User;
use App\Form\PostFormType;
use App\Repository\AccountRepository;
use App\Repository\PostRepository;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    private PostRepository $postRepository;
    private AccountRepository $accountRepository;
    private PaginatorInterface $paginator;
    private FileUploader $fileUploader;
    private EntityManagerInterface $entityManager;

    public function __construct(
        PostRepository $postRepository,
        AccountRepository $accountRepository,
        PaginatorInterface $paginator,
        FileUploader $fileUploader,
        EntityManagerInterface $entityManager
    )
    {
        $this->postRepository = $postRepository;
        $this->accountRepository = $accountRepository;
        $this->paginator = $paginator;
        $this->fileUploader = $fileUploader;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/account", name="app_main_page")
     */
    public function index(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $post = new Post($user);
        /** @var Account $account */
        $account = $user->getAccount();

        $form = $this->createForm(PostFormType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $imageFile */
            $imageFile = $form->get('image')->getData();
            if ($imageFile) {
                $imageFilename = $this->fileUploader->upload($imageFile);
                $post->setImage($imageFilename);
            }
            $post->setDate(date("m.d.Y, H:i"));

            $this->entityManager->persist($post);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_main_page');
        }

        $query = $this->postRepository->getPost($user);
        $pagination = $this->paginator->paginate(
            $query,
            $request->query->getInt('page', 1), 7
        );

        return $this->render('post/index.html.twig', [
            'postForm' => $form->createView(),
            'pagination' => $pagination,
            'account' => $account
        ]);
    }
}
