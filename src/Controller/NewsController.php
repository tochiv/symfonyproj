<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends AbstractController
{
    private UserRepository $userRepository;
    private PaginatorInterface $paginator;

    public function __construct(UserRepository $userRepository, PaginatorInterface $paginator)
    {
        $this->userRepository = $userRepository;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/news", name="app_news")
     */
    public function index(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $query = $this->userRepository->getSubscribedPosts($user);
        $pagination = $this->paginator->paginate(
            $query,
            $request->query->getInt('page', 1), 15,
        [
            'distinct' => false,
        ]
        );

        return $this->render('news/index.html.twig', [
            'pagination' => $pagination
        ]);
    }
}
